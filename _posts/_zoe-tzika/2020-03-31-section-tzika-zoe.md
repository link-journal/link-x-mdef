---
title: Hyperdomestic x Interconnected
author: Zoe
category: Networks
layout: Documentation
---


# Hyperdomestic x Interconnected



![Letter](../_zoe-tzika/_assets/Networking-01.jpg)


This is a collective note-taking effort to document and learn from the organising of solidarity in response to the urgency of care by the outbreak of Covid-19.


**It offers practical guidance and inspiration to self-organising and helping each other.**


It is a way to articulate demands to shift our societies from capitalism, consumerism, patriarchy and racism to societies centered on collectivising the interdependent well-being of humans and nature.


**All we have is each other.**



## Acts of solidarity


![Letter](../_zoe-tzika/_assets/Networking-04.jpg)

At Napoli people made the **Panoro solidare**, based on a local tradition.
Acts of solidarity sprung spontaneously as people were realizing that there are all together in this.




## Finding ways of connecting in a world that demands distancing

**Getting to know who lives close to us and finding ways to offer help to people in home isolation**

This situation demands physical distance from us, but this doesn't mean that we shouldn't try to find bridges to connect with each other.

Now more than ever, the digital tools can create links and platforms of discussion in order to offer solutions to very tangible issues.

This virus makes us realize how precarious we all are, and that we are all together in this.

Let the people in need know that you can help them by posting a note in the building entrance, leaving a note under or next to their door, or ringing them up.

Leave a phone number as a contact and basic information about yourself or your group to establish initial trust.

Let them know in your note that if they themselves might not need assistance that they can inform their friends and family who might need assistance that you are offering help.



**Letter to your neighbours**

![Letter](../_zoe-tzika/_assets/Networking-05.jpg)



**Ways of helping:**

- Talk with the person in home isolation following the guidelines of protection, what you can do to help them in their daily life?
- What food needs do they have? Can you order that online and have it delivered, or is it better that you deliver the provisions yourself?
- Do they have the medicines they need? Do they need a prescription? Can you pick up their medication from the pharmacy? Do they have masks, soap and desinfectants?
- Do they have a thermometer and fever and cough medicine?
- Can they prepare a meal or do they need help? Can you make them a meal? Or instead arrange to have food delivered to them from a soup or solidarity kitchen?
- Do they need to have their rubbish put out or mail collected?
- Do they have a house pet? Does it need to be walked?
- Do they have money? Do they have cash? Can they pay online? Do they have a trusted person who can withdraw cash for them? Do they need financial assistance?



## Organizing in groups


![Example image](https://lp-cms-production.imgix.net/image_browser/France%20COVID-19%20Tribute.jpg?auto=format&fit=crop&q=40&sharp=10&vib=20&ixlib=react-8.6.4&w=1678)



1. Organize yourselves in groups in your building or along your street/ neighborhood to identify the people who need support and help with things such as: grocery shopping, childcare, etc
2. In order to organize, you can: convene meetings of staircases or street sections, visit all floors in your building to know the situation in each flat, install an information board at your entrance
3. It is important to let everyone know that they are not alone, that they can count on the help of the neighborhood
4. Generate communication channels between everyone in the building or the street.
5. Both digital (via mobile phone) and physical (a sign at the entrance can be enabled to keep everyone informed and share needs)
5. All of this should be done following the health care tips to avoid spreading the infection

The main directions that the groups are offering help are:
- Aid for people in isolation
- Food resources
- Psycological support
- Legal support (housing & work)
- Financial support

![Letter](../_zoe-tzika/_assets/Networking-03.jpg)

## The sprung of solidarity groups

Since the outbreak of the virus the appearance of mutual aid groups was rapid and massive. In the countries that suffer from many cases the response of the society was immense.

Local groups appeared in each area, asking the people to get organized and to join forces in order to help the ones that would need it.

This manifested the possibility of citizen assemblies and collective acting.

![Letter](../_zoe-tzika/_assets/Networking-07.jpg)

![Letter](../_zoe-tzika/_assets/Networking-08.jpg)

![Letter](../_zoe-tzika/_assets/Networking-09.jpg)




## Documentation of the efforts / directories / resources


![Letter](../_zoe-tzika/_assets/Networking-10.jpg)


![Letter](../_zoe-tzika/_assets/Networking-11.jpg)




## Mapping the mutual aid groups in Spain


![Letter](../_zoe-tzika/_assets/Networking-12.jpg)



## Organizing for alternative futures

The pandemic is likely to push an already unstable global economy out of control, triggering measures to restore capitalist accumulation that might, as we have seen in past, bring about further reductions to the public care system, dismantling of working rights and conditions, discouraging civic life and deepening inequality and poverty.
The result might set back efforts to counter climate change that might lead to new disasters.
Against these prospects, the new ways of organising could effectively lead to an empowerment of the citizens in order to make political claims.

We are living through a time of deep transformation that will impact our collective future beyond the emergency of containing the epidemic. It is both a time of acceleration, a time of uncertainty, a time of suspension. The shape of what is to come is not prescribed, but it will greatly depend upon our joint reflection and capacity to organise political actions. There will be an urge to simply “go back to normal, quickly”. And that tendency or hope, however understandable on the psychological level, will need to be collectively addressed, and also healed.

**It is also a time where we have been provided with a peek into an alternative future.**

The challenge today and in the next period of time is and will be how to keep the surge in solidarity provoked by this multiple crisis - that is, as a force motivating people to come together and make demands for systemic changes in public health and for the environment, for moving beyond the capitalist  growth, speed and consumption. Experiences and examples linked here are taken from different places in the world, in the spirit of internationalism and translocalism, which might be one of the lessons we re-learn from the virus.

![Letter](../_zoe-tzika/_assets/Networking-13.jpg)
