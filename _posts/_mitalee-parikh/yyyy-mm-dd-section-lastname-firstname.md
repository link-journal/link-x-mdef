---
title: Decisions of today
author: Mitalee Parikh
category: public health
layout: post
---

#### Below is a template of tasks for MDEF students, replace the dummy text with your designs and ideas 👇

# Decisions of today

#### Mitalee Parikh

**The decisions of today reflect the actions and repercussions of tomorrow**

**This project outlines how the decisions of today, using global examples from healthcare, policymaking, philanthropy and community, will sculpt and shape our society of tomorrow, for the better or worse**

---

## Healthcare

Effective triage is the appropriate response to ensure that in spite of a severe mismatch between supply and demand allocation of resources is fair. In an overwhelmed system with critically unwell patients, doctors must decide which patients get oxygen, intensive care, both, or neither.

Fundamentally, this is a question of ethics and distributive justice. In answering this question Italy has opted for a utilitarian approach: “the principle of maximizing benefits for the largest number”. That allocation must be towards “those patients with the highest chance of therapeutic success”. Indeed, American ethicists have also suggested that utilitarianism in some form is the best response to rationing in the face of coronavirus.

Healthcare professionals carry stress unprecedented during this time. They are highly vulnerable themselves, but also put their family in jeopardy or are isolated from them for long periods of time. This has taken mental toll on thousands of doctors, nurses in the line of duty.

https://blogs.bmj.com/medical-ethics/2020/03/16/the-moral-cost-of-coronavirus/

## Policy-making

![Example Image](_posts/_mitalee-parikh/_assets/picture1.png)
![Example Image](_posts/_mitalee-parikh/_assets/picture2.png)
![Example Image](_posts/_mitalee-parikh/_assets/picture3.png)
![Example Image](_posts/_mitalee-parikh/_assets/picture4.png)
![Example Image](_posts/_mitalee-parikh/_assets/picture5.png)

## Philanthropy
DONATIONS
VOLUNTEERING
COMMUNITY INITIATIVES

![Example Image](_posts/_mitalee-parikh/_assets/picture10.png)

## Community  

Hindu groups in India have accused a Muslim evangelical event of turning New Delhi into a hotbed of coronavirus infection, threatening to snowball the discussions into a Hindu-Muslim war of words.
Several Hindu groups have demanded the arrest and punishment of organizers of the mid-March conference in the Nizamuddin area after police last week moved hundreds to hospitals with symptoms of Covid-19.
The influential Vishwa Hindu Parishad (VHP) has described the Nizamuddin meet as a "corona factory" and demanded that Muslims "should come forward and close with immediate effect all the mosques in India that are still open."
"A corona-infected deceased must be mandatorily cremated irrespective of religious affiliation to stop the spread of the infection," it said in a call to follow the Hindu custom of cremation instead of the Muslim tradition of burial.
The Hindu comments came after police on March 31 completed evacuation of more than 2,000 Muslim preachers from the headquarters of Islamic missionary group Tablighi Jamaat in Nizamuddin. An estimated 40 percent of them now show signs of Covid-19.

SOCIAL BIAS
XENOPHOBIA
![Example Image](_posts/_mitalee-parikh/_assets/picture7.png)

RACISM
![Example Image](_posts/_mitalee-parikh/_assets/picture8.png)

SOCIAL STIGMAS
![Example Image](_posts/_mitalee-parikh/_assets/picture9.png)

DENIAL IN TESTING
JANTA CURFEW
![Example Image](_posts/_mitalee-parikh/_assets/picture6.png)

#### Questions & Feedback

[mitalee.parikh@students.iaac.net](nhu.tram.veronica.tran@iaac.net)
