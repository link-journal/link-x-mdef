---
title: Virus prevention for all
author: Ching-Chia Renn
category: Education
layout: post
---


# Virus prevention for all

#### Author (Ching-Chia Renn)

**In the period of the virus outbreak, there are many hygiene concepts, preventive actions, and rules that all people should respect. A good way of communication can be not only easily understood, but also can convince others and be complied with. If all the information is well told and followed, the epidemic will be controlled better, therefore having a good way of delivering information is more important than we thought. What is the better way to convey the ideas of virus prevention for everyone to easily understand and pay attention to the ideas? How can we inform our communities rapidly and effectively around the world to stay safe and health during an outbreak of a pandemic disease?**


There were many different ways of advocacy of virus prevention popping up everywhere during the outbreak of COVID-19. Not only governments and some organizations, but also individuals started having actions to give their communities hygiene concepts, preventive actions and rules to follow. However, it is never easy to disseminate and promote an idea to others because people need to suffer before that they can learn a lesson from suffering all the time. Thus, how to make people believe the idea, realize the importance of it and put it in practice will be a question to think about. But in crisis, prevention is always better than cure, it would be too late if everyone was getting sick. That’s why we should have a good way of communication for all of us to understand and follow the actions of prevention.

I found many different ways of delivering information, and there are two ideas of the way to do that, one is through digital media and the other is in the physical environment. The first idea is spreading the information throughout digital media by using posters, pictures, illustrations, and videos etc. And the second one is having some additions in the physical environment, there are indicator lines and signs, barrier objects, or the arrangement of space to remind people the actions they should have or even restrict some behaviors.

In these two ideas, I would take social distancing as an example of advocacy of virus prevention to discuss different ways of communication to disseminate the prevention in digital media and the physical environment. I think that social distancing is a great example to discuss because it seems to be not very important in virus prevention and it is forgettable and easily accidentally violated, but it’s unexpectedly powerful, therefore there were various ways trying to convey the information hard, positively and seriously.

---

## Idea 1: In digital media

In digital media, there are posters, pictures, illustrations, and videos etc. to deliver the preventive action and I simply classified them into three ideas.
A good way of communication can be not only easily understood, but also can convince others and be complied with.

![Example Image](_posts/_chingchia-renn/_assets/handshake.jpg)

The followings are the better ways:

1. Not saying what cannot do, but telling what could/should do

![Example Image](_posts/_chingchia-renn/_assets/social1.gif)

![Example Image](_posts/_chingchia-renn/_assets/social2.jpg)

![Example Image](_posts/_chingchia-renn/_assets/4-7.jpg)

![Example Image](_posts/_chingchia-renn/_assets/social3.jpg)

2. Data display, comparison, and description

![Example Image](_posts/_chingchia-renn/_assets/4-4.jpg)

![Example Image](_posts/_chingchia-renn/_assets/4-5.jpg)

![Example Image](_posts/_chingchia-renn/_assets/4-1.gif)

![Example Image](_posts/_chingchia-renn/_assets/4-2.gif)

3. Sense of humor

![Example Image](_posts/_chingchia-renn/_assets/5-2.png)

![Example Image](_posts/_chingchia-renn/_assets/5-3.jpg)

![Example Image](_posts/_chingchia-renn/_assets/5-1.png)

![Example Image](_posts/_chingchia-renn/_assets/6-3.jpg)


## Idea 2: In the physical environment

In the physical environment, there are indicator lines and signs, barrier objects, or the arrangement of space to remind people the actions they should have or even restrict some behaviors.

1. Indicator (line/sign)

![Example Image](_posts/_chingchia-renn/_assets/1-6.jpg)

![Example Image](_posts/_chingchia-renn/_assets/1-9.jpg)

![Example Image](_posts/_chingchia-renn/_assets/1-7.jpg)

![Example Image](_posts/_chingchia-renn/_assets/1-8.jpg)

2. Barrier object

![Example Image](_posts/_chingchia-renn/_assets/3-5.jpg)

![Example Image](_posts/_chingchia-renn/_assets/3-4.jpg)

3. Spatial arrangement

![Example Image](_posts/_chingchia-renn/_assets/3-3.jpg)

![Example Image](_posts/_chingchia-renn/_assets/1-5.jpg)


#### Questions & Feedback

[ching.chia.renn@student.iaac.net](ching.chia.renn@student.iaac.net)
