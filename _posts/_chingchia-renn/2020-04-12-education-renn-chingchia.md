---
title: Virus prevention for all
author: Ching-Chia Renn
category: education
layout: post
---

![Example Image](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQtqWcZ1DsZonkhP-2dDGMr0TyYjkAHf2k4thMyA6Rjbgk8NGbm&usqp=CAU)

# Virus prevention for ALL

A better way of communication

#### Author (Ching-Chia Renn)

**In the period of the virus outbreak, there are many hygiene concepts, preventive actions, and rules that all people should respect.
A good way of communication can be not only easily understood, but also can convince others and be complied with.
If all the information is well told and followed, the epidemic will be controlled better, therefore having a good way of delivering information is more important than we thought.
What is the better way to convey the ideas of virus prevention for everyone to easily understand and pay attention to the ideas?
How can we inform our communities rapidly and effectively around the world to stay safe and health during an outbreak of a pandemic disease?**

There were many different ways of advocacy of virus prevention popping up everywhere during the outbreak of COVID-19.
Not only governments and some organizations, but also individuals started having actions to give their communities hygiene concepts, preventive actions, and rules to follow.
However, it is never easy to disseminate and promote an idea to others because people need to suffer before that they can learn a lesson from suffering all the time.
Thus, how to make people believe the idea, realize the importance of it and put it in practice will be a question to think about. But in crisis, prevention is always better than cure, it would be too late if everyone was getting sick.
That’s why we should have a good way of communication for all of us to understand and follow the actions of prevention.

I found many different ways of delivering information, and there are two ideas of the way to do that, one is through digital media and the other is in the physical environment.
The first idea is spreading the information throughout digital media by using posters, pictures, illustrations, and videos, etc.
And the second one is having some additions in the physical environment, there are indicator lines and signs, barrier objects, or the arrangement of space to remind people the actions they should follow or even restrict some behaviors.

In these two ideas, I would take social distancing as an example of advocacy of virus prevention to discuss different ways of communication to disseminate the prevention in digital media and the physical environment.
I think that social distancing is a great example to discuss because it seems to be not very important in virus prevention and it is forgettable and easily accidentally violated, but it’s unexpectedly powerful,
therefore there were various ways trying to convey the information hard, positively and seriously.

---

## Idea 1: In Digital Media

In digital media, there are posters, pictures, illustrations, images, and videos, etc. to deliver the preventive action and I simply classified them into three ideas.

![Example Image](https://thinkthink.files.wordpress.com/2010/10/flu-season.jpg?w=788)

When you see the image like the one above, you might have some questions or problems, “No hand-shaking, what else can I do?”, “Does it really matter?”, “I did it accidentally…”

This kind of image is not a very good way to disseminate preventive ideas.

A good way of communication can be not only easily understood, but also can convince others and be complied with.

The followings would be better ways:

1. Not say what cannot do, but telling what could/should do
2. Data display, comparison, and description
3. Sense of humor

#### 1. Not say what cannot do, but telling what could/should do

![Example Image](https://upload.wikimedia.org/wikipedia/commons/a/ac/Covid-19-Handshake-Alternatives-v3.gif)

(How to socially distance without seeming rude. Site: https://menafn.com/1099900352/Nice-to-meet-you-now-back-off-How-to-socially-distance-without-seeming-rude)

![Example Image](https://i2-prod.mirror.co.uk/incoming/article21635747.ece/ALTERNATES/s615b/0_coronavirus.jpg)

(Handshake alternatives. Site: https://menafn.com/1099873082/Why-Singapores-coronavirus-response-worked-and-what-we-can-all-learn)

![Example Image](https://bozell.com/wp-content/content/layout_Social_Distancing-scaled.jpg)

(Social Distancing for Social People (and Others). Site: https://bozell.com/social-distancing-for-social-people-and-others/)

From the examples can be seen that they all showed people what else could do without hand-shaking in social situations.
When people received this information, they learned alternative ways to maintain their normal lives.

#### 2. Data display, comparison, and description

![Example Image](https://www.visualcapitalist.com/wp-content/uploads/2020/03/The-Math-Behind-Social-Distancing-Shareable-Final.jpg)

(The math behind social distancing. Site: https://www.visualcapitalist.com/the-math-behind-social-distancing/?utm_content=buffer5e76c&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer)

![Example Image](https://pbs.twimg.com/media/ETf5Tt8UMAADESt.jpg)

![Example Image](https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Covid-19-Transmission-graphic-01.gif/615px-Covid-19-Transmission-graphic-01.gif)

(https://commons.wikimedia.org/wiki/File:Covid-19-Transmission-graphic-01.gif)

![Example Image](_posts/_chingchia-renn/_assets/4-3.gif)

(The mathematics of social distancing. Site: https://www.science.org.au/curious/people-medicine/mathematics-social-distancing)

![Example Image](https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Katapult_importance_social_distancing.gif/350px-Katapult_importance_social_distancing.gif)

(https://commons.wikimedia.org/wiki/File:Katapult_importance_social_distancing.gif)

The examples above all show some simple data and comparison of different scenarios.
These displayed the effects and results from different actions which give people evidence to believe the preventive action.
People can learn from data and the comparison to understand how important preventive action is.  

#### 3. Sense of humor

![Example Image](https://cdn.cnn.com/cnnnext/dam/assets/200326121014-20200326-social-distancing-corporate-logos-split-gfx-exlarge-169.jpg)

(Iconic brands redesign their logos in Coronavirus solidarity. Site: https://winkreport.com/iconic-brands-like-mcdonalds-redesign-their-logos-in-coronavirus-solidarity/)

![Example Image](https://wowlavie-aws.hmgcdn.com/file/article_all/source/logo_0.jpg)

![Example Image](https://media.timeout.com/images/105622851/630/472/image.jpg)

(Time Out is now Time In temporarily while we in Barcelona do more staying in than going out. Site: https://www.timeout.com/barcelona/things-to-do/time-in-why-weve-changed-our-logo-for-now)

![Example Image](https://static.designboom.com/wp-content/uploads/2020/03/artists-viral-safety-match-matchstick-video-social-distancing-03-17-designboom-600.gif)

(Los Angeles-based artists Juan Declan and Valentina Izaguirre have created an animated video shot that uses matches to illustrate the impact of social distancing. Site: https://www.designboom.com/art/artists-viral-safety-match-matchstick-video-social-distancing-03-17-2020/)

![Example Image](https://pbs.twimg.com/media/ESa26glXYAABfRO.jpg)

(Iranian actor shows how to ‘musically’ wash hands to protect against Coronavirus. Site: https://www.youtube.com/watch?v=cgaL-XV_7BM&feature=emb_title)

![Example Image](https://i.ytimg.com/vi/iVC01GxFwDg/hq720.jpg?sqp=-oaymwEhCK4FEIIDSFryq4qpAxMIARUAAAAAGAElAADIQj0AgKJD&rs=AOn4CLC6VQ5crEq597bJziHn4DXVe6IeJg)

(Disney Characters in Quarantine. Site: https://www.youtube.com/watch?t=89s&v=iVC01GxFwDg)

Interesting and funny things can impress people more and make people remember.
In addition, when people saw an interesting image, video or a funny thing happened, they would be more eager to share this thing, then information and message can be better delivered.

#### Reflect

In digital media, I discovered that the learning process is more like people learned first then they tried to practice what they learned and apply.
There is no mandatory restriction of behavior in this way of information transmission, and people have the freedom to decide whether they would like to follow the preventive action or not.


## Idea 2: In the Physical Environment

In the physical environment, there are indicator lines and signs, barrier objects,
or the arrangement of space to guide and remind people what actions and instructions they should follow or even restrict their behaviors in some cases,
in order to deliver the information about preventive ideas.

Take social distancing as an example, there are three kinds of ways to convey the information, which is keeping a distance from each other, in the physical environment:

1. Indicator (line/sign)
2. Barrier object
3. Spatial arrangement

#### 1. Indicator (line/sign)

It is a way to guide people how to do it.

![Example Image](https://pbs.twimg.com/media/EUQOOTtUYAAqKy-.jpg)

(MRT increases the distance to control COVID-19. Site: https://www.komchadluek.net/news/economic/424000)

![Example Image](https://pbs.twimg.com/media/ET-BAZwWkAECsw_.jpg)

(Spar enacts social distancing measures in stores. Site: https://www.thegrocer.co.uk/convenience/spar-enacts-social-distancing-measures-in-stores/603440.article)

![Example Image](https://pbs.twimg.com/media/ETZx5X3XQAIEGOW.jpg)

(Interior floor sticker. Site: https://www.asaprint.sk/samolepka-podlahova)

![Example Image](https://img-s-msn-com.akamaized.net/tenant/amp/entityid/BB11wxWh.img?h=0&w=720&m=6&q=60&u=t&o=f&l=f)

(Stickers to protect the social distance of a hospital in the elevator in Thailand. Site: https://www.viralsfeedpro.com/news/view-1534.html)

#### 2. Barrier object

People were restricted but also reminded.

![Example Image](https://i2-prod.irishmirror.ie/whats-on/food-drink-news/article21724944.ece/ALTERNATES/s1200b/89924912_3317803444899823_5057155903199903744_o.jpg)

(Lidl Ireland. Site: https://twitter.com/lidl_ireland/status/1240736425810763777)

![Example Image](https://newsimg.ftv.com.tw/summernotefiles/News/113a74e5-46f8-485a-8ce8-fced6ba8feff.jpg)

(Adding a transparent acrylic partition to each table in the student cafeteria to avoid the spread of the virus. Site: https://www.ftvnews.com.tw/news/detail/2020303U11M1)

#### 3. Spatial arrangement

People were guided and reminded.

![Example Image](https://cdn.iwnsvg.com/uploads/2020/04/Physical-distancing.gif)

(Physical distancing. Site: https://www.iwnsvg.com/2020/04/02/opposition-urges-churches-to-take-physical-distancing-seriously/)

![Example Image](https://nestia-food-obs-ap-southeast-3.nestia.com/202004/01/974d6dc1b3d3f06fc574b646be404339.jpg)

(Safe distancing markers pasted across Singapore. Site: https://www.asiaone.com/digital/new-instagram-pages-dedicated-safe-distancing-markers-pasted-across-singapore)

#### Reflect

In the physical environment, I felt that the learning process is more like people made an action and practiced first then they learned from the practices.
There are different kinds of indicators, prompts or instructions to guide and remind people.
When people saw these indicators, prompts, or instructions and followed them in conscious or unconscious, people would kind of learn from their practices and actions(, but may not realize).


#### How to make people learn and practice?

It’s always said, "Knowing is one thing, taking in action is another." It's not easy to have both.

The ways to convey information in digital media are more focused on the content of information, what message would be delivered, and how people can get and learn from the message.
However, the ways to communicate in the physical environment are more focused on guiding people’s actions and trying to let people practice and follow the instructions at the moment.

Should we learn before the action or after?
Or maybe the ways to deliver information in digital media and in the physical environment should cooperate with each other so that people can learn and practice at the same time.


## Speculation

The advocacy of preventive action is kind of to change people’s behavior. However, changing behaviors is always not an easy thing to do.

In the future, maybe these messages will be hidden in everyday objects in our lives. They might not be aware, but we will be influenced by them for sure.

#### New fashion in social distancing era

![Example Image](https://media.distractify.com/brand-img/9WV3yYOdB/480x252/coronavirus-social-distancing-tactics-1584037495758.jpg)
![Example Image](https://www.chinapress.com.my/wp-content/uploads/2020/03/CR200327tpm05.jpg)

(Coming up with creative ways to practice social distancing. Site: https://www.stratmorgroup.com/mortgagesat_tips/what-can-covid-19-teach-us-about-safeguarding-the-customer-experience/)

![Example Image](https://d3i6fh83elv35t.cloudfront.net/static/2020/03/crinolines.jpg)

(The fashionable history of social distancing. Site: https://www.pbs.org/newshour/arts/the-fashionable-history-of-social-distancing)

In order to maintain social distancing, prevent making mistakes accidentally, and protect ourselves, there will be new ways of dressing.

![Example Image](_posts/_chingchia-renn/_assets/s1-1.png)
![Example Image](_posts/_chingchia-renn/_assets/s1-2.png)

(Credited by Ching-Chia Renn)

![Example Image](_posts/_chingchia-renn/_assets/s2-1.png)
![Example Image](_posts/_chingchia-renn/_assets/s2-2.png)

(Credited by Ching-Chia Renn)

#### New ways to perceive the world

![Example Image](https://static01.nyt.com/images/2020/03/24/science/24BRODYISOLATION/24BRODYISOLATION-superJumbo.jpg)

(Take Steps to Counter the Loneliness of Social Distancing. Site: https://www.nytimes.com/2020/03/23/well/family/coronavirus-loneliness-isolation-social-distancing-elderly.html)

![Example Image](http://tnr.com.tw/images/NewsImage/%E9%98%B2%E7%96%AB%E4%B9%9D%E5%AE%AE%E6%A0%BC-ppt%E7%89%88%E6%9C%AC-%E6%96%B0%E4%B8%8A%E5%9C%8B%E5%B0%8F.jpg)

(Bingo game for everyday preventive action and hygiene behavior. Site: http://tnr.com.tw/txtsemple.aspx?id=31181)

Playability promotes the willingness to practice preventive action.

What if there will be a sensor on our body to detect others and objects around us, then send the data to our mobile application, it could be a game for people to keep a better social distance from each other.
The game would be designed to make people follow the certain rules.

![Example Image](_posts/_chingchia-renn/_assets/s3.png)

(Credited by Ching-Chia Renn)

#### Questions & Feedback

[nhu.tram.veronica.tran@iaac.net](nhu.tram.veronica.tran@iaac.net)

[julia.danae.bertolaso@iaac.net](julia.danae.bertolaso@iaac.net)
