---
title: Living in an infected world
author: Isaul Garcia
category: Health
layout: post
---

# Living in an infected world

---

#### Author Isaul Garcia

Covid-19 is most dangerous due to its ease of infection. Like any other virus, it can be transferred by touching surfaces or through air particles. Anyone or anything around you could be infected.

During the warning state people were coming up with ways to minimize the infection. The current state of emergency suggests people to stay at home. As for the people who still have to visit a facility to work or shop for groceries, they might encounter themselves with some of these initiatives. Perhaps these procedures were meant to reduce the chance of the virus spreading, but after this event, it could also mean that we would have to maintain these precautions to finally dissipate the virus.

---

## Handless Door Handles

The virus lives in surfaces that have come in contact with an infected individual. It is certain that door handles are one of the most vulnerable surfaces we encounter everyday. We open a countless number of doors on a daily basis by using our hands, but when it comes to a virus threat the simple act of opening a door could be the way to let the virus come into your body. When it comes to prevention, resources like gloves are limited. Perhaps creating alternate preventive methods, to better distribute resources, is essential. Here are some examples of door handle extensions that avoid using hands.

#### Examples

This very first image comes from a company named Materialise that focuses on 3D printing solutions. They’ve designed this door handle extension that avoids using your hands, and use your arm instead, to open doors with this specific type of handle. Their design is open to download and produce, yet of course you need to have a 3D printer at disposition.

There have been multiple examples of people that have come up with similar solutions, also using 3D printing.

![Door Handle Extension 1](19000.png)

In these next two images you can see examples of other types of extensions that not necessarily rely on the production of a new object. You could use plastic straps to attach an object that has a similar shape and still avoid using hands, or you could carry around your own hanger shaped object that lets you open these kinds of doors without touch.

![Door Handle Extension 2](19001.png)

Other solutions to encompass other types of door handles or a different approach in general, like opening the door with your foot. All of these were created specifically to prevent COVID-19.

![Door Handle Extension 3](19002.png)

#### Resources

Links to the 3D files of some of these models.

![Door Handle Extension Models](19005.png)

[Hands Free Architecture](https://www.handsfreearchitecture.com/)
[Materialise Hands-Free](https://www.materialise.com/en/hands-free-door-opener)
[My Mini Factory](https://www.myminifactory.com/object/3d-print-hands-free-round-door-handle-bolt-on-under-2-dia-115908)
[Thingi Verse 1](https://www.thingiverse.com/thing:4238848)
[Thingi Verse 2](https://www.thingiverse.com/thing:4229655)


#### Post-Pandemic life

We still live under the uncertainty of what life after this event will be like. These practices might have been created mainly to prevent the spread of the virus during the warning state, and now we are practicing complete self quarantining. But what does it mean for things to come back to normal. After this, it's probable that we might still have to live with some sort of prevention state to completely dissipate the spread of the virus. Regardless of having to keep these actions right after COVID-19, this solutions could be mimicked in the future, if it were to happen again, and quickly maintain a level of control over the spread.

I believe that these will serve a reference to how we reacted to the event. It is more common for companies to own 3D printers or to even know of someone that has one. Maybe in the future it’ll be more common or maybe there’s something far more advanced. In any case I believe that what we can benefit from is the analysis made from these people into these specific types of door handles. I made some illustrations that mapped the shape and the action of the door handle, and the second one with the repurpose of the handless door handle.

![Door Handle Extension Analysis](19006.png)
![Door Handle Extension Attempt](19007.png)

Do less harm, use your arm.

#### Questions & Feedback

[isaul.josue.garcia@student.iaac.net](isaul.josue.garcia@students.iaac.net)
