---
title: Resilience or Resistance
author: Hala Amer
category: Education
layout: post
---

# The Shape of Necessity

# Delivery Services

#### Hala Amer


**Introductory statement.** Whether one is living through isolation, social-distancing, or quarantine there is an ongoing fight for normalization. To bring the normal to the abnormal.. To make this style of life the new normal.. To design a new normal.. Many jumped into action to find solutions and become one of the first to survive economically. Businesses are jumping to different solutions and the end users are accepting to all and any changes. Many companies have quickly developed apps or websites, have rented out cars and drivers, and have made many changes as a quick solution to the pandemic's affect on their business. Many people jumped opted for using DIY techniques to be creative or to live. And as end users, we accepted it all without questioning any of it.

**Descriptive text about sub-focus.** As Qatar had always valued comfort, delivery services have existed in respect to food and groceries. However, after the rise of the quarantine the delivery services stepped up to bring more comfort to those at home. In Jordan, the poverty levels are high but they still value comfort. I have used examples of services from both countries. The services began to also focus on medication, toys, electronics, alcohol, and different types of services.

What had once been recreational became a necessity. For that reason, this repository has been made to take note of what services have transformed to work through delivery.

---

## Idea 1: Bread

Bread is seen as the basic necessity in Middle Eastern countries and others, if a family has bread available then they can survive harsh times.

After the lockdown in Jordan, the government enlisted a schedule to allow for bread to be delivered to each household to encourage staying at home. In the course of two days, a delivery schedule was established, buses and bus drivers were recruited, and panaderos were tasked with specific number of bags of bread to have ready.
The rounds began with buses stopping at different neighborhoods and a police force standing outside each bus to make sure people followed the required rules of standing in line. The rules included leaving distances of 4 meters between people and wearing masks and gloves. The results varied from different neighborhoods.
Depending on the neighborhoods, some people followed the rules and some did not. In some locations, panic and havoc took over when people started attacking the buses.

Action | ...
------------ | -------------
Bread Delivery | ![Bread Delivery](bus-bread.jpeg)  Delivery of the bread on buses
Following Rules | ![Following Rules](following.jpeg)  Military making sure citizens follow the mandated 4 meters
Not Following Rules | ![Not Following Rules](bread.jpeg) Failure of citizens realizing the danger in the panic

##### Personal Thoughts

The purpose of the bread delivery was to allow the citizens who are more in need to feel safe staying home as many live on a da-to-day salary basis. However, through misinformation and the fear of a more tangible enemy: hunger.

## Idea 2: Groceries

Mouneh - an app that allows you to find the closest grocery stores to your location to minimize wandering outdoors. Once the location is found one needs to print out a paper that is signed with the house location and the supermarkets location, to make sure that the citizens are following the shortest routes. Mouneh.jo also allows you to see which nearby supermarkets are licensed to be delivering during the lockdown.

[Mouneh](https://www.mouneh.jo/) | ![Mouneh App](mouneh.JPG)
------------ | -------------

Baqala- an app that delivers fresh groceries, fish, and meat that began to function shortly before the rise of the pandemic and is now successful. "Baqaala is a super-fast, ultra-convenient Doha grocery delivery service that’s setting new standards of excellence in customer service. We bring fresh groceries, fresh fish & meat from stores to your door, making shopping in Doha a quick and hassle-free experience. Whether it’s your weekly grocery shopping , or a couple of items that you need like milk, eggs, fruits, vegetables,fresh fish, meat, water.. etc , Baqaala will deliver. Forget queues, or spending ages finding somewhere to park. All you need for grocery delivery in Doha is the Baqaala app on your mobile phone."

[Baqala](https://www.baqaala.com/) | ![Baqala App](baqala.JPG)
------------ | -------------

##### Personal Thoughts

I can't help but wonder whether the existence of these apps increases the waste produced. Through the convenience of having everything delivered, many trips that you naturally would have avoided will no longer be avoided and the amount of produce and products used will naturally increase.

## Idea 3: Medicine

WellCare Pharmacy is an "online pharmacy" where you upload your prescription that will then be reviewed by a pharmacist and then delivered to your doorstep. This pharmacy functions in both online and physical, there are a chain of WellCare Pharmacies and when you order online they deliver it from the closest location to you.

[WellCare Pharmacy](https://www.onlinepharmacy.qa/) | ![WellCare](wellcare.JPG)
------------ | -------------

##### Personal Thoughts

The idea of the online pharmacy can be developed further through a more blockchain method. The prescriptions uploaded could be sent out to an "available" pharmacist whether they are inside a pharmacy or working from home. The pharmacist can then approve and resend it out to an available certified deliverer. In the method that they are currently working requires a number of pharmacists to still be physically in the pharmacies, which to an extent defies the purpose of the lockdown.

## Idea 4: Alcohol

 Beginning before the pandemic and thriving through the lockdown is an alcohol delivery application in Jordan. "Botler is Jordan’s largest digital marketplace for alcohol, connecting drinkers to suppliers, giving our customers the best way to shop for their drinks."

 [BotlerJo Website](https://botlerjo.com/) | ![App Store](botlerjo.jpeg)
 ------------ | -------------

 ##### Personal Thoughts

Does the availability of alcohol delivery increase the chances of people gathering in secret? As Jordan has been extremely strict with their quarantine and have declared the use of the National Defense Law, it is difficult to wander in the city but not impossible and through Instagram stories we see it is still happening.

The National Defense Law gives the government sweeping powers to impose a state of emergency.


### Previously existing *SERVICES* delivery in Jordan and Qatar
#### CareemBox
[Careem](https://www.careem.com/en-ae/) | ![Careem](careem.JPG)
------------ | -------------

"Careem is the internet platform for the greater Middle East region. A pioneer of the region's ride-hailing economy, Careem is expanding services across is platform to include mass transportation, delivery and payments to become the region's everyday SuperApp."

The Careem app, which started off as a Middle Eastern Uber, expanded to other services including CareemBox. CareemBox allows app users to order a car for a pickup-drop-off service, including food, personal belongings, and even groceries.

#### eButler
[eButler](https://www.e-butler.com/) | ![eButler](ebutler.JPG)
------------ | -------------

"EButler is the most reliable services providers, having an All-in-One app on your phone that provides you with everything you need in your personal life."

eButler was a startup that understood the nature of the lifestyles in Qatar and decided to work accordingly. Through eButler you can make doctor appointments, speak to doctors through the app, order food, have your car cleaned, and even book a meeting with "sharks".

Through the lockdown, many services are suddenly not as easy to approach and so the usage of eButler has increased.

 ##### Personal Anecdote
 While in quarantine, my family and I wanted to set up the house with activities that we can do over the course of 2 weeks and since we were not allowed to be wandering the streets between hypermarkets we decided to call on eButler to find us the products we were looking for, deliver them to us, and even sterilize them.

 This personal experience adds to the thoughts and questions I have began with. We decided to set up the house to include as many "normal" activities as possible. We fought to bring in our strand of normality to the new way of life.


 ### Final thoughts

 The rise of deliveries will continue as even more businesses find ways to adapt and that rise is not necessarily beneficial or harmful. The way to approach it is by taking care of the employees who will be meeting different people daily. Many of us opt for ordering to decrease our chances of meeting people who could be infected yet we do not think of the safety of the delivery driver.

 There will always be the resistors. There will always be people who refuse to make changes and continue to find ways to live in their normal, like going out, meeting friends, or even touching the produce at the supermarket barehanded.

While collecting the examples a question came up to mind: **what is a necessity in this age?** What are we willing to give up for the sake of "saving the other" whether it be the delivery driver or the family members. Who did the resistors think they were fooling?

#### Questions & Discussions

[Website - Hala Amer](https://halaalzawaydeh.gitlab.io/hala.alazawaydeh/index.html)

[Email - hala.alzawaydeh@students.iaac.net](hala.alzawaydeh@students.iaac.net)
