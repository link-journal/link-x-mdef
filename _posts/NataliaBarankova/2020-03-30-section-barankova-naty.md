---
title: (N)ever changing reality 
author: Natalia Barankova 
category: Extended Reality 
layout: post
---

# (N)ever Changing Reality 
### Introduction to Project by Natalia Barankova 
The spaces influence our thinking to a great extend. From the psychology of architecture, we can learn, that for example, the colours of the walls and height of the ceilings make us think in different ways. While the low ceiling rooms makes us better at solving anagrams involving confinement, the high ceiling rooms make us excel at puzzles in which the answer touches on the theme of freedom. 
However, what happens if your environment does not change? Our homes become a prison, where the place we eat becomes the place we sleep, and the place we sleep becomes the place we to work. 
The objects around us become too familiar and somehow invisible to a human brain. Do you remember the math problem you could not solve even after going through it again and again? Your mind got accustomed to seeing the problem in the same way. What happened when you got out of desk? 
The lack of new sensory experiences from outside world feeds on our creativity, problem-solving abilities as well as productivity. Human brains crave for change; it craves for “new”. And so we consume, buy, and binge on TV. Could COVID lead us to find new ways of experiencing reality, enhancing our environment through blurring the physical and digital? Could Covid-19 make us want to watch less TV and scroll through Facebook, and learn better and faster through drawing us into more engaging, changing reality? 

![Sample Video](_posts/NataliaBarankova/assets/workk.MP4)

### Reshaping and rebuilding reality
Now, in times of Coronavirus, we can observe people enhancing their home environments to bring the outside experiences inside. Imagination serve as a powerful tool as the floor can be turned into ice court or a treadmill and a row of glasses into hurdle race. Despite rebuilding the environments, there is an emerging tendency towards interacting with objects around us in other ways than normal, making use of their ignored properties, using their properties to benefit our necessities as well as reimagine what they really are and experience a bit of novelty. 

![Sample Image](_posts/NataliaBarankova/assets/glasses.PNG)
![Sample Video](_posts/NataliaBarankova/assets/outside2.MP4)
![Sample Video](_posts/NataliaBarankova/assets/v09044870000bpsipqcb0k2g3ouargd0.MP4)
![Sample Video](_posts/NataliaBarankova/assets/outside.MP4)

### Phygital

New Macro Trend in online entertainment is based on blurring the boundaries between physical vs digital and real vs imaginative world. We can notice the need to have a power to manipulate/reshape the stable world around us. The reality comes to play and becomes entertaining through the manipulative and flattening interfaces, added sound and mixed images.  These tendencies
are being muliplied in times of CoronaVirus. 

![Sample Image](_posts/NataliaBarankova/assets/blabla.jpg)
![Sample Video](_posts/NataliaBarankova/assets/flute.MP4)
![Sample Video](_posts/NataliaBarankova/assets/tv.MOV)

### Augmented Reality 

These tendencies are pointing towards the technology that has been developed while ago, yet not fully reached its audience and potential. Covid-19 makes AR and VR technologies more relevant then ever before. 
Augmented reality (AR) is an interactive experience of a real-world environment where the objects that reside in the real world are enhanced by computer-generated perceptual information. Augmented reality alters one's ongoing perception of a real-world environment, whereas virtual reality completely replaces
the user's real-world environment with a simulated one. 
 ![Sample Image](_posts/NataliaBarankova/assets/covidar.jpg)
 Augmented reality helping an outdoor photographer to fight anxieties and bring a bit of outside inside. 
 ![Sample Image](_posts/NataliaBarankova/assets/tiger.jpg)
 
 The market if full of diverse AR applications, free to download or to be downloaded for a small fee. Augmented characters and creations help people to "make washing hands a bit more exciting", turn table into a playground for augmented players, turn space into canvas, or simply not to feel lonely. 
 The AR is an escape from the ordinary into the phantastic realities that can be entered easily, through the screen of the phone. Can you imagine the use of AR beyond the entertaning aspect?
 ![Example Image](_posts/NataliaBarankova/assets/ar.jpg)

Number of AR applcations has been created in times of CoronaVirus to help people deal, and teach how to behave as well as to help them escape the oversaturated and fearful COVID world. With these apps you can train yourself to avoid handshakes and high-fives, be aware of the virus as well as social distancing!
 ![Example Image](_posts/NataliaBarankova/assets/hands.mp4)
 ![Example Image](_posts/NataliaBarankova/assets/corona.jpg)

For example this app, can help you enter the door and dissapear in into the unknown. Do not worry you have a chance to come back...if you want to. 
 ![Sample Video](_posts/NataliaBarankova/assets/app.mov)
 
 I have experienced AR on my own skill. I have tried to use the app for education Holon, Ikea app to decorate my house and add some furniture, placed animals in my kitchen using different apps, as well as trained drawing SketchAR.  I must say the experience has been very refreshing. I was going creative with it, for example placing annimals in their correct environments (placing penguins into the fridge, fish into the pan). It made me move and walk around the whole house, experiencing it in new way and allowed me to explore within the defined environment.
  ![Sample Video](_posts/NataliaBarankova/assets/Bez_názvu.mp4)
    ![Sample Video](_posts/NataliaBarankova/assets/ikea.mp4)
     ![Example Image](_posts/NataliaBarankova/assets/holon.jpg)
     
However, one thing I found annoying was looking on AR through small screen of my phone. With augmented reality glasses, the experience would become much more immersive and natural!

### What else? Education!  

Despite the entertainment that is the most visible feature of AR, this tool can be used to help solve the dilemma of remote education, to enhance the learning experience of children, make it more interactive and sensory. Contrasting to lectures and reading books, AR allows for 3D visualisation and interaction, making study fun and therefore more effective as the attention span is longer. In next epidemics, we can not allow the home education to become a series of dull skype calls, viewing the classmates through flat 2D screen, with limited tools and prototypes  and possibility to learn through doing!
Even though the school of your child, has not yet acquired AR, it does not mean you can not include it in your life. For inspiration I have created a sample timetable with AR tools that are free to download. 
     ![Example Image](_posts/NataliaBarankova/assets/schedule.jpg)

### What else? Work!  
Working remotely has become complicated for many proffesionals. This is true especially for proffesions, requiring the physical aspect - prototyping, printing, moving objects.. 
With AR your work experience can become much more then just looking for alternatives and moving the deadlines and meetings. Use AR to build prototypes and see them in 3D to test the function and material needed, before lab opens and you can fabricate again. 
Create visualisations of the interior and send them to your client so he can imagine his new environment remade. Help your psychology patients to face or overcome their fears from comfort of their home. 
     ![Example Image](_posts/NataliaBarankova/assets/work.jpg)
     We might need to stay in isolation much longer...maybe the "work from home" will become the new normal. Would you consider purchasing the new AR headsets to keep your proffesion and practice alive? 
https://www.youtube.com/watch?v=eqFqtAJMtYE 
https://www.youtube.com/watch?v=YAiqGwExoYA
### Future 

What if instead of actually buying a new couch, you could just upload an image of it onto your old one? In the future, moving houses will be just a matter of downloading and uploading the furniture. Millennials are more experience-focused and less materialistic than previous generations..
Why own a fancy, expensive wall hanging when you could just project it? 
By using AR, we will be able to superimpose digital architecture and design onto any surface, then add a few pets, plants, aliens, zombies, or whatever else you like roaming around your living room. AR visors will provide virtual TVs without the need for a physical screen, so you wouldn’t even need to buy those: You could lie in bed and watch Netflix on your ceiling instead.
![Example Image](_posts/NataliaBarankova/assets/Snímka_obrazovky_2020-04-12_o_16.34.05.png)

What consequences this could have on sustainability? Are we going to prefer the digital over physical? Can VR and AR help us imagine sustainable speculative futures to  allow humanity to embody, live, and understand a future it wants and to work towards it? Such a desired future won’t feel as distant or abstract.
![Example Image](_posts/NataliaBarankova/assets/Snímka_obrazovky_2020-04-12_o_16.38.47.png)
Nike created this digital sofa "to make design accessible for everyone, encouraging everyone to stay home, but still be able to experience design,"Crosby Studios said on its Instagram.

Can we AR make people more sensible towards news and sustainable emergency? Melting ice and dying animals are far from us, making us depersonalised and detached from the emotional and emphatetic feelings towards it as if this was not happening. 
Can AR we make them more aware/prepared for the next epidemics?
Can we make technology sensing virus and making invisible visible ?

Will COVID-19 change the fate of AR and VR? 
And create beneficial tools for more interactive education…
cutting CO2 from work trips through VR conferences 
Allow for less consumption through simulation..?  

In the case of next pandemic, VR conferences could become a tool for people to meet despite physically being there, and make the experience more "alive" and interactive. 
"Will self-imposed isolation due to the threat of COVID-19 change the fortunes of VR and AR? It’s easy to see the current crisis as the necessary spark for new technologies that allow people to collaborate more deeply. For example, Microsoft had already positioned its AR-based HoloLens as a way for everyone from surgeons to industrial workers to collaborate over long distances. And Facebook’s Oculus is already set up for people to meet and chat in a virtual space; it just needs customers to actually take it up on the offer." (https://insights.dice.com/2020/03/25/covid-19-change-fate-virtual-reality-augmented-reality/)
![Example Image](_posts/NataliaBarankova/assets/hololens.jpg)
![Example Image](_posts/NataliaBarankova/assets/conference.jpg)

Despite the visible optimism raised in this chapter, I feel a need to stay neutral and point at the scary future this might bring. What if COVID-19 is going to shape the world in such a way people will be too fearful to go outside almost at all? Nowadays, anything can be ordered online, and extended realities would allow us to bring any experience inside. What it the fear from the infected world will keep us locked in our sterile ever changing environments? 
What if we will not create a reality with no need to buy new stuff, thanks to being able to experience it virtually, but a reality where brands develop AR/VR tools that will make us crave the new even more? What if local dissapears from the dictionary and everything we buy is shipped around the whole world? 
What if AR and VR is going to become the new weapon towards minds of humanity?
Today, there are children whose version of what chicken means is limited to the idea of chicken wings wrapped in foil on the shelves of supermarkets. Poor level of physicality, cutting out the 90 percent of what chicken is, yet still materiality. What would raising new generation in digital world mean? 
Are we going to end up living with boxes on our hands, constantly projecting the reality we want, or need, and make us incapable to solve problems in real life, to experience the real nature..? What if exnteded reality is going to become the only reality in which we can experience today's endangered species? 

![Example Image](_posts/NataliaBarankova/assets/jungle.jpg)










