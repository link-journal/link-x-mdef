---
title: Mental Health issues of medical workers
author: Ergina Restou
category: Health
layout: post
---

# The Shape of Necessity


## Introduction to Project by Tomas

The Master in Design for Emergent Futures is proposing a revision of the publication made by the Cuban government during the 1990's, when the island was going through the special period after the fall of the Iron Curtain. MDEF's tutors and students will work in a new repository and publication, understanding that the world is now in a new "special period", after the release of the COVID-19.
Knowing the limitations of mobility that the world is experiencing, there is a dramatic change in logistics and supply chains, as well as new ways to access knowledge. Right now, we are more connected than ever and we are more isolated than ever at the same time. The current global pandemic is opening up opportunities for hyperlocal interventions that can help people to live confinement in much better conditions while increasing long term resilience.
The exercise will consist of mapping and documenting existing practices that are happening now at the domestic scale in the world, and which people are using to solve local needs, by following open source projects or existing documentation. The work will be organized following the categories of the book "Con Nuestros Propios Esfuerzos".
These categories will be assigned to students by MDEF tutors, and they will serve as a starting point to organize their work for the week. A second part of the exercise will consist of imagining near-future scenarios, starting from January 2021, and proposing the evolution of these categories and projects in the context of the Post-Corona society. We want our students to identify not only the current situation of confinement but also being able to anticipate the world that could emerge out of this forced transition. Starting from January 2021, we want our students to make an effort to identify what these collective practices or this domestic practices could look like in six or nine months.
Ernesto Oroza, who is going to be a collaborator in this week's exercise, will participate in the collective efforts by inspiring students in how to curate and identify which kind of solutions or which kind of recipes are being developed and shared around the world as never before.
Objectives:


- to document do-it-yourself practices that are emerging at the domestic space from people that are solving basic needs related with food, fixtures of electronics, clothing, or fixing anything that is in their homes


- to curate together with MDEF tutors a collection of solutions and projects that are offering opportunities to address the current reconfiguration of the everyday life of humans in the world of confinement


- to speculate about possible near futures after the Corona pandemic, starting in January 2021


The output expected from this project this week project is to have a living repository of solutions or ideas or creative ideas for people to implement other domestic spaces as well as to think about how a new productive society might be emerging from this current crisis of the Coronavirus, and how we can envision this productive society to finally emerge from 2020 as a transition year.

#### Below is a template of tasks for MDEF students, replace the dummy text with your designs and ideas 👇

# Impact of COVID-19 on the physical and mental health of medical personnel
#### Author Ergina Restou

<b>Introduction </b>

<p>
Coronavirus is a sneaky disease that no matter how careful someone is, they can get it. It is usual for doctors who treat patients who have or may have the virus, to be afraid. Fear, to a certain extent, can protect you, so you have to find the balance between fear and doing your job well. As much as they train to deal with severe conditions, what is happening now is entirely unprecedented. They are forced to fear each other but also to be isolated from their families and friends. Learning how many health workers have died, they are right to think they might be the next.
</p>

<p>
Physical and psychological pressure can lead them to make wrong decisions and actions, both for the patients and for their own health. It is proven that no state, no matter how organized, could have foreseen a mass turnout of patients with serious problems, especially respiratory problems. Although most hospitals are equipped with means of dealing with severe cases under normal conditions, the pressure from several seriously ill patients is such, that there is insufficient means and nursing staff to treat patients. There are shortages of machinery, masks, gloves, uniforms. Excessive exposure of health staff to patients with the disease along with physical and mental fatigue automatically leads them to the highest risk group to become ill. Hundreds of doctors and nurses around the world have lost their lives in the battle with COVID-19 trying to abide by their vow.
</p>
<p>
In many occasions doctors, under time pressure, are forced to decide who to give a chance to fight for their lives. Unfortunately, the elders in many cases are left to fight this alone. The mental burden that these people are asked to lift and manage is unreal and it is also one that will leave them with marks that will take much longer to heal than what masks leave.
</p>
---

## HOW IT STARTED

![sample image](assets/introimage.jpg)
According to this research (https://www.medrxiv.org/content/10.1101/2020.02.20.20025338v2.full.pdf) performed at the ground zero of the pandemic :
<p> Half of participants (50.4%) had symptoms of depression, while 44.6% had anxiety symptoms, 34% had symptoms of insomnia, and 71.5% showed signs of distress.

## This information lead me to research more about the impact of fighting a pandemic on medical workers

### Here are some of the challenges they face :

- Experts said doctors and nurses risk having post-traumatic stress disorder. They could also develop "compassion fatigue," which is characterized as a diminished ability to empathize or feel compassion for others because the caregiver is physically and emotionally exhausted.

- First responders face an increased risk of experiencing behavioural health issues including mental illnesses and substance use disorders. Fear of being seen as weak or not up to the job of a first responder keeps many from seeking help. Responders can build their resilience by increasing awareness about risk factors and warning signs, talking with each other, and using healthy coping strategies.

- "And there are no more surgeons, urologists, orthopedists, we are only doctors who suddenly become part of a single team to face this tsunami that has overwhelmed us."

- "I expect it'll have quite a lot of psychological impact on people, not only doctors but nurses and everyone else who works within [the critical care] team," said Dr. Laura Hawryluck, the critical care response team lead at Toronto Western Hospital."

info collected from (https://www.cbc.ca/news/health/covid19-doctors-ptsd-1.5507548
)

### Through the research i found that:
#### - it is very important that they know they are not alone and that other people like them face the same challenges
#### - to show to people that they too are struggling mentally
#### - to create platforms that support them

#### For that reason i decided to collect in one place all the above and so, create a space called  Medical Chart" for them to visit whenever they feel they need to.


#  Medical Chart

### Familiar faces

![sample image](assets/faces3.png)
![sample image](assets/faces2.png)
![sample image](assets/faces4.png)
![sample image](assets/faces5.png)
![sample image](assets/faces6.png)

[https://www.theatlantic.com/health/archive/2020/03/coronavirus-italy-photos-doctors-and-nurses/608671/ ]

### Let me tell you a story

#### Deciding who gets to live

D'Ambrosio said: "If we understand the patient has a severe health issue to the point of having no chance [to live] and we need to give the bed or divert resources to someone who has more chances to survive, [then] this is a choice that — despite being ethically hard to accept — from a clinical point of view can be done to give the possibility to survive [to someone] compared to someone who would have zero chance."

![Sample Video](_posts/_ergina-restou/assets/video1.mp4)


"Anesthetists – despite them playing it down a little bit on the media – have to choose who they attach to the machine for ventilation, and who they won’t attach to the machines"

#### Watching people die alone

"I pulled out the phone and called her on video. They said goodbye. Shortly after she left. By now I have a long list of video calls. I call it the farewell list. I hope they give us mini iPads, three or four would be enough, not to let them die alone."

![sample video](_posts/_ergina-restou/assets/video2.mp4)

#### physical exhaustion

"There are no more shifts, no more hours. Social life is suspended for us. We no longer see our families for fear of infecting them. Some of us have already become infected despite the protocols.

![sample video](_posts/_ergina-restou/assets/video5.mp4)

https://www.weforum.org/agenda/2020/03/suddenly-the-er-is-collapsing-a-doctors-stark-warning-from-italys-coronavirus-epicentre/

#### Missing my family

![sample video](_posts/_ergina-restou/assets/video6.mp4)


#### Everyday struggle

![sample video](_posts/_ergina-restou/assets/video7.mp4)
![sample video](https://www.youtube.com/watch?v=q28fwuC-tUM&feature=emb_logo)

![sample video](https://www.youtube.com/watch?v=q28fwuC-tUM&feature=emb_logo)

![sample video](_posts/_ergina-restou/assets/video8.mp4)
![sample video](https://www.youtube.com/watch?time_continue=1&v=xrsuoEo_GYs&feature=emb_logo)

![sample video](_posts/_ergina-restou/assets//video3.mp4)
![sample video](https://www.youtube.com/watch?v=ilQDwaS8-6c&t=97s)

https://time.com/5807918/health-care-workers-selfies-coronavirus-covid-19/
https://www.nytimes.com/2020/04/03/opinion/coronavirus-icu-nurse.html?auth=login-google&referringSource=articleShare

#### No equipment

![sample image](assets/medicine.png)


![sample video](_posts/_ergina-restou/assets/video4.mp4)
![sample video](https://www.youtube.com/watch?v=Qx1XCAeePWA&t=2s)

# Necessity is the mother of adaptation

## innovative ideas
![sample video](_posts/_ergina-restou/assets/video8.mp4)
![sample video](_posts/_ergina-restou/assets/video9.mp4)
![sample videos](https://www.youtube.com/watch?time_continue=1&v=uClq978oohY&feature=emb_logo)

https://gulfnews.com/world/asia/india/covid-19-indian-doctors-fight-coronavirus-with-raincoats-helmets-amid-lack-of-equipment-1.1585643687197

## Coping mechanisms

### it is ok to have fun

![sample video](_posts/_ergina-restou/assets/video10.mp4)
![sample video](_posts/_ergina-restou/assets/video11.mp4)
![sample video](_posts/_ergina-restou/assets/video12.mp4)





## Here you can find pdfs with information on the signs that indicate you may be suffering from PTSD

[2020.02.20.20025338v2.full.pdf](2020.02.20.20025338v2.full.pdf)

[9789241548922_eng.pdf](9789241548922_eng.pdf)

[mental-health-considerations.pdf](mental-health-considerations.pdf)

[pfa_field_operations_guide.pdf](pfa_field_operations_guide.pdf)




## Here you can find information on how to deal with the PTSD an epidemic crisis leaves behind

[https://www.jwatch.org/na51190/2020/03/27/mental-health-effects-covid-19-healthcare-workers-china](https://www.jwatch.org/na51190/2020/03/27/mental-health-effects-covid-19-healthcare-workers-china)

[https://www.msf.org/covid-19-urgent-help-needed-across-european-borders-protect-medical-staff](https://www.msf.org/covid-19-urgent-help-needed-across-european-borders-protect-medical-staff)

[https://www.cbc.ca/news/health/covid19-doctors-ptsd-1.5507548](https://www.cbc.ca/news/health/covid19-doctors-ptsd-1.5507548)

[https://www.ptsd.va.gov/covid/COVID_healthcare_workers.asp](https://www.ptsd.va.gov/covid/COVID_healthcare_workers.asp)

[https://www.vox.com/2020/3/10/21171217/coronavirus-covid-19-italy-hospitals](https://www.vox.com/2020/3/10/21171217/coronavirus-covid-19-italy-hospitals)

[https://www.mdmag.com/medical-news/covid-19-affects-mental-health-nurses-frontline-more](https://www.mdmag.com/medical-news/covid-19-affects-mental-health-nurses-frontline-more)

[https://wwmt.com/news/local/doctors-nurses-risk-mental-health-issues-amid-covid-19-outbreak](https://wwmt.com/news/local/doctors-nurses-risk-mental-health-issues-amid-covid-19-outbreak)

[https://emergency.cdc.gov/coping/responders.asp](https://emergency.cdc.gov/coping/responders.asp)

[https://www.samhsa.gov/dtac/disaster-responders](https://www.samhsa.gov/dtac/disaster-responders)

[https://www.cdc.gov/coronavirus/2019-ncov/daily-life-coping/managing-stress-anxiety.html](https://www.cdc.gov/coronavirus/2019-ncov/daily-life-coping/managing-stress-anxiety.html)



## THANK YOU
![sample image](thankyou.png)

## Some art for you

![sample image](assets/art1.png)
![sample image](assets/art2.png)





```

#### Questions & Feedback



[georgia.restou@students.iaac.net] (georgia.restou@students.iaac.net)
