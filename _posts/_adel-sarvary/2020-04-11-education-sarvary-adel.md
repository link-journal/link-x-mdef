---
title: Directing a homeschool from the kitchen
author: Adel Sarvary
category: education
layout: post
---

# The Shape of Necessity

### Introduction to Project by Tomas
The Master in Design for Emergent Futures is proposing a revision of the publication made by the Cuban government during the 1990's, when the island was going through the special period after the fall of the Iron Curtain. MDEF's tutors and students will work in a new repository and publication, understanding that the world is now in a new "special period", after the release of the COVID-19.

Knowing the limitations of mobility that the world is experiencing, there is a dramatic change in logistics and supply chains, as well as new ways to access knowledge. Right now, we are more connected than ever and we are more isolated than ever at the same time. The current global pandemic is opening up opportunities for hyperlocal interventions that can help people to live confinement in much better conditions while increasing long term resilience.

The exercise will consist of mapping and documenting existing practices that are happening now at the domestic scale in the world, and which people are using to solve local needs, by following open source projects or existing documentation. The work will be organized following the categories of the book "Con Nuestros Propios Esfuerzos".

These categories will be assigned to students by MDEF tutors, and they will serve as a starting point to organize their work for the week. A second part of the exercise will consist of imagining near-future scenarios, starting from January 2021, and proposing the evolution of these categories and projects in the context of the Post-Corona society. We want our students to identify not only the current situation of confinement but also being able to anticipate the world that could emerge out of this forced transition. Starting from January 2021, we want our students to make an effort to identify what these collective practices or this domestic practices could look like in six or nine months.

Ernesto Oroza, who is going to be a collaborator in this week's exercise, will participate in the collective efforts by inspiring students in how to curate and identify which kind of solutions or which kind of recipes are being developed and shared around the world as never before.

Objectives:

- to document do-it-yourself practices that are emerging at the domestic space from people that are solving basic needs related with food, fixtures of electronics, clothing, or fixing anything that is in their homes

- to curate together with MDEF tutors a collection of solutions and projects that are offering opportunities to address the current reconfiguration of the everyday life of humans in the world of confinement

- to speculate about possible near futures after the Corona pandemic, starting in January 2021

The output expected from this project this week project is to have a living repository of solutions or ideas or creative ideas for people to implement other domestic spaces as well as to think about how a new productive society might be emerging from this current crisis of the Coronavirus, and how we can envision this productive society to finally emerge from 2020 as a transition year.

---

#### Below is a template of tasks for MDEF students, replace the dummy text with your designs and ideas 👇

# Directing a homeschool from the kitchen 

#### Adel Sarvary

**The 2019–20 coronavirus pandemic has affected educational systems worldwide, leading to the widespread closures of schools and universities, forcing nearly 90% of the world's student population to shift to emergency distance instruction system. This situation puts many educators and parents under pressure, not to mention students themselves. Moreover, not all schools are prepared to offer virtual classes, and not all students are equipped to learn online, which undermines the quality of education at all levels. To somehow balance this 'borderline' education, social and mass media have been flooded by online learning tools, resources and tricks for homeschooling techniques.** 

Despite the abundance of online resources, parents of school-aged children often don't have time to go through or prepare for extra activities, especially most of which involve extra screen-time. Additionally, most households don't have enough - or any - computers for all their children, let alone printers to print activity sheets or buy other required tools. 
In the intersection of lack of time and lack of technology / financial resources, a new domestic classroom is emerging - a common area in which families spend time together every day: the kitchen. 
Learning about life through the kitchen is nothing new, but due to the global school closures, the world is now discovering the potential of this domestic learning space to complete the school syllabus.
The focus of this chapter is to demonstrate a new wave of creative homeschooling approach through the kitchen: how parents worldwide are creating a practice using simple kitchen tools and machines, and also food and all the knowledge related to deliver the schools' syllabus and educate their children while preparing and eating breakfast, lunch and dinner during the times of confinement. Most activities can be related to STEM education, but there are more and more ideas appearing (re-appearing) in social media related to any other STEM-related or unrelated topics such as geography, literature, grammar, arts, history and economics.
---

## Idea 1: Art

- Block printing with fruits and vegetables.
- Making edible paint with fruits.
- Making salt dough for sculpturing.
- Building art installations using kitchen tools, fruits and vegetables.
- Making music using kitchen tools.

![](../_adel-sarvary/_assets/arts.jpg)
![](../_adel-sarvary/_assets/arts2.png)
![](../_adel-sarvary/_assets/arts3.mov)
![](../_adel-sarvary/_assets/arts4.png)

## Idea 2: Geography

- Looking at the lables and discussing where the ingredients are coming from.
- Discussing spices and relating them to countries / regions.
- Discussing typical traditional food of different nations.
- Demonstrating the Earth's structure by dough.
- Making lunar ice.

![](../_adel-sarvary/_assets/geography.png)
![](../_adel-sarvary/_assets/geography2.png)
![](../_adel-sarvary/_assets/geography3.png)

## Idea 3: Economics  

- Analysing store bills.
- Making shopping list with price estimations, then comparing it with the real prices.
- Setting up a food store or a restaurant, and running the business with the family members as customers.
- Discussing the logistics through the supply chain of different ingredients, food and spices.

![](../_adel-sarvary/_assets/economics.PNG)
![](../_adel-sarvary/_assets/economics2.png)

## Idea 4: History

- Using food to build historical buildings / monuments while learning about their stories.
- Learning about the history of kitchen, the origin and history of certain food and ingredients as well as equipment.
- Discussing food distribution during wars and crises throughout history.

![](../_adel-sarvary/_assets/history.PNG)

## Idea 5: Literature and Grammar

- Relating characters of novels, as well as novelists and poets to food and the taste of certain food.
- Practicing spelling in flour.
- Writing recipes that rhyme.

![](../_adel-sarvary/_assets/literature.PNG)
![](../_adel-sarvary/_assets/literature2.PNG)

![](../_adel-sarvary/_assets/grammar.png)

## Idea 6: Maths, Geometry, Physics and Technology

- Measuring and scaling ingredients.
- Practicing the concept of proportions.
- Solving formulas in playful ways using kitchen tools and food products / packaging.
- Building shapes, angles, forms using spaghetti / toothpicks, weighing and measuring structures.
- Learning about the sources of energy and how kitchen equipment work: stove, microwave oven, mixer, fridge, dishwasher etc.
- Creating vacuum science experiments, lava lamp, experiments with pepper and soap, convection.
- Learning about curves and reflexions.

![](../_adel-sarvary/_assets/maths.MP4)
![](../_adel-sarvary/_assets/maths.png)
![](../_adel-sarvary/_assets/maths2.png)
![](../_adel-sarvary/_assets/maths3.png)
![](../_adel-sarvary/_assets/maths4.png)
![](../_adel-sarvary/_assets/maths5.png)
![](../_adel-sarvary/_assets/maths6.png)

![](../_adel-sarvary/_assets/geometry.png)
![](../_adel-sarvary/_assets/geometry2.png)

![](../_adel-sarvary/_assets/physics.png)
![](../_adel-sarvary/_assets/physics2.mov)
![](../_adel-sarvary/_assets/physics3.JPG)

![]https://dayton247now.com/news/morning-show/fun-experiments-your-kids-can-do-while-stuck-at-home

## Idea 7: Biology and Environmental Studies

- Discussing through experiments and activities how fruits and vegetables grow.
- Categorizing food into food groups, learning about the differences.
- Talking about the human body and categorizing food using the senses.
- Discussing hygene.
- Discussing agricultural processes and composting.
- Discussing recycling and package waste management.
- Discussing how digestion works.

![](../_adel-sarvary/_assets/biology.PNG)
![](../_adel-sarvary/_assets/biology2.png)
![](../_adel-sarvary/_assets/biology3.png)
![](../_adel-sarvary/_assets/biology4.png)
![](../_adel-sarvary/_assets/biology5.png)
![](../_adel-sarvary/_assets/chemistry3.png)
![](../_adel-sarvary/_assets/chemistry5.png)
![](../_adel-sarvary/_assets/chemistry6.png)

## Idea 8: Chemistry

- Experiments with baking soda.
- Experiments with eggs.
- Making bioplastic.

![](../_adel-sarvary/_assets/chemistry.PNG)
![](../_adel-sarvary/_assets/chemistry2.PNG)

## Speculations

Children adapt to new situations very quickly, but they also remember very well of past experiences. If they experience kitchen as a place where it's fun to learn, where there is a lot do discover, where there are lots of tools to do creative activities, there is a high chance that they will look at it differently than as now (mainly, a place where mama and papa are cooking and make them sit tight and eat food that is considered healthy). The science and economy behind foodmaking and eating is of historical importance, therefore transforming the kitchen into a key homeschooling classroom during the global lockdown has a gamechanging potential for after-coronavirus times.

## Resources

- Friends and acquaintances in Hungary
- Facebook of friends
- FAMILY LOCKDOWN TIPS & IDEAS Facebook group: https://www.facebook.com/groups/871176893326326/
- Instagram of friends

#### Questions & Feedback

[adel.kitti.sarvary@students.iaac.net] (adel.kitti.sarvary@students.iaac.net)

[nhu.tram.veronica.tran@iaac.net](nhu.tram.veronica.tran@iaac.net)

[julia.danae.bertolaso@iaac.net](julia.danae.bertolaso@iaac.net)