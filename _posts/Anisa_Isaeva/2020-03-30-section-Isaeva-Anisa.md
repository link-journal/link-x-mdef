---
title: Music industry in a remote mode
author: Anisa Isaeva
category: future of work
layout: post
---

# The Shape of Necessity

### Introduction to Project by Tomas
The Master in Design for Emergent Futures is proposing a revision of the publication made by the Cuban government during the 1990's, when the island was going through the special period after the fall of the Iron Curtain. MDEF's tutors and students will work in a new repository and publication, understanding that the world is now in a new "special period", after the release of the COVID-19.

Knowing the limitations of mobility that the world is experiencing, there is a dramatic change in logistics and supply chains, as well as new ways to access knowledge. Right now, we are more connected than ever and we are more isolated than ever at the same time. The current global pandemic is opening up opportunities for hyperlocal interventions that can help people to live confinement in much better conditions while increasing long term resilience.

The exercise will consist of mapping and documenting existing practices that are happening now at the domestic scale in the world, and which people are using to solve local needs, by following open source projects or existing documentation. The work will be organized following the categories of the book "Con Nuestros Propios Esfuerzos".

These categories will be assigned to students by MDEF tutors, and they will serve as a starting point to organize their work for the week. A second part of the exercise will consist of imagining near-future scenarios, starting from January 2021, and proposing the evolution of these categories and projects in the context of the Post-Corona society. We want our students to identify not only the current situation of confinement but also being able to anticipate the world that could emerge out of this forced transition. Starting from January 2021, we want our students to make an effort to identify what these collective practices or this domestic practices could look like in six or nine months.

Ernesto Oroza, who is going to be a collaborator in this week's exercise, will participate in the collective efforts by inspiring students in how to curate and identify which kind of solutions or which kind of recipes are being developed and shared around the world as never before.

Objectives:

- to document do-it-yourself practices that are emerging at the domestic space from people that are solving basic needs related with food, fixtures of electronics, clothing, or fixing anything that is in their homes

- to curate together with MDEF tutors a collection of solutions and projects that are offering opportunities to address the current reconfiguration of the everyday life of humans in the world of confinement

- to speculate about possible near futures after the Corona pandemic, starting in January 2021

The output expected from this project this week project is to have a living repository of solutions or ideas or creative ideas for people to implement other domestic spaces as well as to think about how a new productive society might be emerging from this current crisis of the Coronavirus, and how we can envision this productive society to finally emerge from 2020 as a transition year.

---

# Music industry in a remote mode

#### Anisa Isaeva


#### I did research on what happening with music industry during quarantine. I find it important, because since we spend 24 hours at home 7 days per week, music became a huge emotional support for us, that helps somehow fight with depression and anxiety The brisk pace of coronavirus outbreak is causing chaos in every sector of the entertainment industry. Including music


![Example Image](_posts/Anisa_Isaeva/assets/0.png)
![Example Image](_posts/Anisa_Isaeva/assets/1.png)
![Example Image](_posts/Anisa_Isaeva/assets/2.png)
![Example Image](_posts/Anisa_Isaeva/assets/3.png)
![Example Image](_posts/Anisa_Isaeva/assets/4.png)
![Example Image](_posts/Anisa_Isaeva/assets/5.png)
![Example Image](_posts/Anisa_Isaeva/assets/6.png)
![Example Image](_posts/Anisa_Isaeva/assets/7.png)
![Example Image](_posts/Anisa_Isaeva/assets/8.png)
![Example Image](_posts/Anisa_Isaeva/assets/9.png)
![Example Image](_posts/Anisa_Isaeva/assets/10.png)
![Example Image](_posts/Anisa_Isaeva/assets/11.png)
![Example Image](_posts/Anisa_Isaeva/assets/12.png)
![Example Image](_posts/Anisa_Isaeva/assets/13.png)
![Example Image](_posts/Anisa_Isaeva/assets/14.png)
![Example Image](_posts/Anisa_Isaeva/assets/15.png)
![Example Image](_posts/Anisa_Isaeva/assets/16.png)
![Example Image](_posts/Anisa_Isaeva/assets/17.png)
![Example Image](_posts/Anisa_Isaeva/assets/18.png)
![Example Image](_posts/Anisa_Isaeva/assets/19.png)
![Example Image](_posts/Anisa_Isaeva/assets/20.png)
---



#### Questions & Feedback

[nhu.tram.veronica.tran@iaac.net](nhu.tram.veronica.tran@iaac.net)

[julia.danae.bertolaso@iaac.net](julia.danae.bertolaso@iaac.net)
