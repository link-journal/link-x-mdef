---
title: Art - scapes
author: Maria Dafni Gerodimou
category: Augmented Realites /Arts and Creativity
layout: post
---




---



# Art is where the home is

#### Daphne Gerodimou

**Expanding the borders of Isolation through Creativity.**

My research is focused in creative practices during the isolation time and generally how artists in various artistic disciplines correspond or adapt to the
current normal. Also, I am interested in investigating how the "home" can work as
a creative trigger and in return how can we (professional artists or not)expand our mental and physical space through
creativity.
**

*I will demonstrate in this research existing creative practices that are formed by the isolation period, creative individuals that could inspire with their actions or art, social media platforms of collective art or sharing the work of various artists in isolation whether are dancers, painters, illustrators etc, some open source libraries that offer free content on films, documentaries, books and finally some examples of creative activities that I have personally engaged from home **

---

## Social media connecting artists

 In this period some interesting Instagram accounts have been created in order to gather and share art that has been made that is expressive of the current situation. It is interesting to think why these collaborative platforms didn't exist, or at least weren't that valuable in life before Covid19. Art has always gained its value through sharing but maybe not by surpassing the paradigm of ownership. I think what the current normal is teaching us and also artists all over the world is this urgency of expression which goes beyond proprietary and individual ego.

It feels like creative people are now more prone to share their work, overcoming a bit the fear of judgment or misunderstanding. They see mow, more than ever, their creative expression as a way to connect with others.There hasn't been a time in recent history were the world came that close to sharing a common reality, common fears, anxieties, everyday lifes. Under the Covid19 “umbrella” makes us, artists or not, even more eager to address these issues that we believe more people than ever before can relate, get inspired or contribute.*




*Profiles on Instagram that share art from various people*




![@covidart / Covid art museum / Covid room ](_assets/insta.jpg)



![@covid.dance](_assets/covidance.jpg)




*Personal Account of art lovers being creative and having fun at the same time*

![@covidclassics](_assets/covidclassics.jpg)

*Similar attempts I founf on reddit*

![](_assets/reddit.jpg)






##  The expanded space. The home as creative lab.

*Within this period I believe is interesting to explore how creative people have used their own homes, their surroundings or common objects found in context to get inspired and create their own work.*

*Here are some examples, but believe me there are hundreds of them out there. I suggest some Covid art searching to everyone*

![@beninmadrid  Drawing on actual context](_assets/benin.jpg)

![Common objects still nature](_assets/objects.jpg)

![Photographer using zoom](_assets/zoom.jpg)


## Actions. First person perspective.

*During this time, I also had the need to express myself creatively by simultaneously trying to adapt to this new domestic reality. Since I moved to
Barcelona I had been constantly "running" ,spending my time mostly in school or
in the transition of places and spaces. Being in isolation forced me to develop
a new relationship with my domestic surroundings. I started seeing my home as
new landscape, an island that I could now explore as if time had decelerated and
physical space was more vibrant than ever. Hence, I started to uncover the poetics of the space I wasn't able to call home until then , hunting for creatively triggers like a shipwrecked in an isolated island looking for food.
I am definitely not an artist but I will share here some of my creative attempts to cope with isolation and keep my sanity.*

## The drawings. Covid or context inspired


![Covid](_assets/draw1.jpg)

![The world from my balcony](_assets/draw2.jpg)


## Photogrammetry. Transforming the physicality of domestic reality with digital mediums

![The physical](_assets/photog.jpg)

![The after physical](_assets/photog2.jpg)



## The annotated reality

![Living room](_assets/annot1.jpg)

![Balcony](_assets/annot2.jpg)




## Resources and Initiatives

*As it is expected, due to the current difficulties emerging from isolation, there has been some open source movements in sharing resources and initiatives
to help creatives and artists all over the world. Also a lot of platforms that feature films, documentaries, theatre performances, book clubs or book libraries are now extending their free trials or even sharing their content for free.I will try to share the ones that I have found but I am sure that this list could
updated every day.*

## Here is an initiative from [Firstsite](https://firstsite.uk) that has created and open source creative activity pack with various suggested creative exercises.


![Balcony](_assets/artis.jpg)


## Below are more links with art related resources

[Firstsite](https://firstsite.uk)

[booooooom](https://www.booooooom.com/2020/03/20/covid-19-resources-for-artists/?fbclid=IwAR15Zjmw8bWj_E_El05oEeaPP7A5MzoKg2DNwJnOA1eefT6ZnizE9sia19A)


[Theguardian](https://www.theguardian.com/books/2020/mar/26/the-perfect-time-to-start-how-book-clubs-are-enduring-and-flourishing-during-covid-19)

[Metropolitanopera](https://www.metopera.org/user-information/nightly-met-opera-streams/week-3/)

[Macba Confinement Diary](https://www.macba.cat/en/diary)

[Atlas of the future](https://atlasofthefuture.org/home-alone-together-33-things-to-read-watch-listen-chill-to/)





























#### Questions & Feedback

[nhu.tram.veronica.tran@iaac.net](nhu.tram.veronica.tran@iaac.net)

[julia.danae.bertolaso@iaac.net](julia.danae.bertolaso@iaac.net)
