---
title: Please, no more charts
author: Andrea Bertran
category: Public Health
layout: post
---

# The Shape of Necessity

### Introduction to Project by Tomas
The Master in Design for Emergent Futures is proposing a revision of the publication made by the Cuban government during the 1990's, when the island was going through the special period after the fall of the Iron Curtain. MDEF's tutors and students will work in a new repository and publication, understanding that the world is now in a new "special period", after the release of the COVID-19.

Knowing the limitations of mobility that the world is experiencing, there is a dramatic change in logistics and supply chains, as well as new ways to access knowledge. Right now, we are more connected than ever and we are more isolated than ever at the same time. The current global pandemic is opening up opportunities for hyperlocal interventions that can help people to live confinement in much better conditions while increasing long term resilience.

The exercise will consist of mapping and documenting existing practices that are happening now at the domestic scale in the world, and which people are using to solve local needs, by following open source projects or existing documentation. The work will be organized following the categories of the book "Con Nuestros Propios Esfuerzos".

These categories will be assigned to students by MDEF tutors, and they will serve as a starting point to organize their work for the week. A second part of the exercise will consist of imagining near-future scenarios, starting from January 2021, and proposing the evolution of these categories and projects in the context of the Post-Corona society. We want our students to identify not only the current situation of confinement but also being able to anticipate the world that could emerge out of this forced transition. Starting from January 2021, we want our students to make an effort to identify what these collective practices or this domestic practices could look like in six or nine months.

Ernesto Oroza, who is going to be a collaborator in this week's exercise, will participate in the collective efforts by inspiring students in how to curate and identify which kind of solutions or which kind of recipes are being developed and shared around the world as never before.

Objectives:

- to document do-it-yourself practices that are emerging at the domestic space from people that are solving basic needs related with food, fixtures of electronics, clothing, or fixing anything that is in their homes

- to curate together with MDEF tutors a collection of solutions and projects that are offering opportunities to address the current reconfiguration of the everyday life of humans in the world of confinement

- to speculate about possible near futures after the Corona pandemic, starting in January 2021

The output expected from this project this week project is to have a living repository of solutions or ideas or creative ideas for people to implement other domestic spaces as well as to think about how a new productive society might be emerging from this current crisis of the Coronavirus, and how we can envision this productive society to finally emerge from 2020 as a transition year.

---

#### Below is a template of tasks for MDEF students, replace the dummy text with your designs and ideas 👇

# Please, no more charts

#### Author (Andrea Bertran)

**Introductory statement.**
**~100 characters**
Nowadays, during the current pandemic crisis (COVID19), access to data is critical.

**Descriptive text about sub-focus. ~300 characters**
Many webpages with tools, dashboards, and maps have emerged to show and track the virus's cases worldwide. All of them are fundamental to make people understand and prepare themselves. But the design of graphs should be accurate, conclusive, and straightforward, more than ever. Why? Because charts can be alarmist and therefore build more panic. Information influences people's decisions and reactions to subjects.

I want to point out some things I find essential as a regular user for data visualizations to have during this crisis.
---

## 1. Use of fonts
Please, make your headlines and legends clear. Use different fonts or even the same one but make it  bold to make sure we can rapidly notice what you are talking about.

![Example Image](../_andrea-bertran/assets/04_Reuters Graphics.JPG)
![Example Image](../_andrea-bertran/assets/13_Ventagium Data Consulting.JPG)

## 2. Put us in context
We’ve seen a thousand graphs already, we already know we are living a pandemic crisis. If you want to highlight something in particular, show us through colors (be aware of the ones you are using and their impact on our minds), subtitles, lines or by writing a short annotation of the interest of your graph.

![Example Image](../_andrea-bertran/assets/16_Nico Hahn.JPG)
![Example Image](../_andrea-bertran/assets/11_BBC.JPG)

## 3. If your graph is complex, guide us through the information.

![Example Image](../_andrea-bertran/assets/09_Qventus.JPG)
![Example Image](../_andrea-bertran/assets/12_El Universal_2.JPG)

## 4. Choose your chart wisely
Maybe you’ll avoid confusion if you show the data in another type of chart.

![Example Image](../_andrea-bertran/assets/06_Datawrapper.JPG)
![Example Image](../_andrea-bertran/assets/17_Data Science Plus.JPG)

## 5. The meaning of colors and their tones
Red is super aggressive, it can mean panic, stress or anger. Can you guys please at least change its tone to be less destructive? There is no need to alarm more.

![Example Image](../_andrea-bertran/assets/01_Johns Hopkins Coronavirus Resource Center.JPG)
![Example Image](../_andrea-bertran/assets/15_University of Virginia.JPG)

## 6. Avoid superfluous information
Symbols and images can create more confusion.

![Example Image](../_andrea-bertran/assets/05_SAS Coronavirus Report_2.JPG)
![Example Image](../_andrea-bertran/assets/14_University of Washington.JPG)

## 7. Remind people the good numbers

![Example Image](../_andrea-bertran/assets/03_Our World in Data_2.JPG)
![Example Image](../_andrea-bertran/assets/07_John Coene's Shiny projects.JPG)

## 8. Link your sources
Naming isn’t good enough.

![Example Image](../_andrea-bertran/assets/02_New York Times_2.JPG)
![Example Image](../_andrea-bertran/assets/03_Our World in Data.JPG)

## 9. Think out of the box
What if you relate the info and the impact to other subjects? - The impact on business, events, people loosing their jobs, the influence on the internet as we are all moving to digital spheres, etc...

![Example Video](../_andrea-bertran/assets/brainstorming.mp4)

## Good examples
Media, journalists and dataviz experts, please reconsider over and over again what you want to show during moments of crisis.

A few,  good examples for (hopefully never again) next time.

![Example Image](../_andrea-bertran/assets/10_Covid19UK.JPG)
![Example Image](../_andrea-bertran/assets/08_Information is beautiful.JPG)
![Example Video](../_andrea-bertran/assets/02_New York Times_2.mp4)

## Sources
01_Johns Hopkins Coronavirus Resource Center - https://coronavirus.jhu.edu/map.html
02_New York Times - https://www.nytimes.com/interactive/2020/03/15/business/economy/coronavirus-worker-risk.html?action=click
03_Our World in Data - https://ourworldindata.org/coronavirus#all-charts-preview
04_Reuters Graphics - - https://graphics.reuters.com/CHINA-HEALTH-SOUTHKOREA-CLUSTERS/0100B5G33SB/index.html
05_SAS Coronavirus Report - https://tbub.sas.com/COVID19/
06_Datawrapper - https://blog.datawrapper.de/coronaviruscharts/
07_John Coene's Shiny projects - https://shiny.john-coene.com/coronavirus/
08_Information is beautiful - https://informationisbeautiful.net/visualizations/covid-19-coronavirus-infographic-datapack/
09_Qventus - - https://qventus.com/blog/predicting-the-effects-of-the-covid-pandemic-on-us-health-system-capacity/
10_Covid19UK - https://covid19uk.live/
11_BBC - https://www.bbc.com/news/health-51674743
12_El Universal - https://interactivo.eluniversal.com.mx/2020/coronavirus-llega-a-mexico/
13_Ventagium Data Consulting - https://app.powerbi.com/view?r=eyJrIjoiMTE1N2ExYzEtMThmNy00ZjdkLThiOGMtNzI5MjIwMjkwYjA5IiwidCI6IjFlYjcxYWY1LTBjNzAtNDU1Zi05MDk1LWU4NWU5N2M3MTM3MyIsImMiOjR9
14_University of Washington - https://hgis.uw.edu/virus/
15_University of Virginia - https://nssac.bii.virginia.edu/covid-19/dashboard
16_Nico Hahn - https://nicohahn.shinyapps.io/covid19/
17_Data Science Plus - https://dash.datascienceplus.com/covid19/


```

#### Questions & Feedback

[nhu.tram.veronica.tran@iaac.net](nhu.tram.veronica.tran@iaac.net)

[julia.danae.bertolaso@iaac.net](julia.danae.bertolaso@iaac.net)
