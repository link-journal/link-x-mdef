---
title: work adaptive & tranformation
author: Luang
category: Future of jobs
layout: post
---

### Introduction:
Everyone knows the future will be unpredictable and everything will change very fast. But we didn't expect that it was gonna happen right now! A lot of people have to protect themselves by staying at home so it affects their jobs and their income.Tens of millions face losing jobs and money in escalating coronavirus crisis,Especially the work that has to be associated with people for example restaurant ,Entertainment work,Airline work. So, how do we survive in this situation?

#### Work adaptive by transformation

At this moment I saw some workers try to do everything to get money in this condition. I found something interesting that they try to adapt or change their work to fit the situation.  I saw some cases that change a bit with their job but some cases absolutely change. People turn obstacles into opportunities. All of the above makes a new weird work. That Some works we didn't see before

---
There are case studies that I found.

### consuming industry job
![pic](_posts/_wongsathon/assets/A1.jpg)
During this time, various restaurants are going bankrupt because the covid19 situation makes people stay at home. No one comes to eat at the restaurant. income of the restaurant is going to be declining. Raw ingredients are going to rot. A lot of employees will not have a salary and lose their jobs. At that moment, I found the transformation of them to adapt their jobs with this situation.

1th solution: Delivery Time <br>
A lot of waiter employees have their own motorcycle So they change their role to be a delivery man because people are still hungry in the home. At the same time, they have to protect themselves with a virus. I saw a delivery man of some restaurant use a mascot costume to protect themselves and also make smiles at customers who see.

2nd solution: Shop in the car <br>
If a customer can’t come to the shop, the shop has to come to the customer. Some of them come up with another solution, they transform their car to be the shop or Grocery to sell food and useful products.  But if you want to use this solution, you need to use a car.

3rd solution: Delivering Parcels <br>
If you do not prefer to use transportation. I found another solution. Someone tries to sell something by delivering parcels. So they can sell food, cookies, masks but anyway they have to protect the product from bumps or breaks.

All of solution, above  can adapt with product not just food

---

### Entertainment jobs
![pic](_posts/_wongsathon/assets/A2.jpg)
The jobs that are affected by covid19 are Entertainment jobs because this kind of jobs have to work closely with many people. For example Jobs in cinema, musicians, events etc. now they have to stop everything because we have to make social distance. And stay in the house. So I found case study to solve this problem and make them to make opportunity to get income on this time

1th solution: Drive-in cinemas <br>
drive-in cinemas, people still need to watch movies in cinema but they can’t because we have to keep social distance. I found a solution that is drive-in cinemas. People can go to watch movies and stay safe in their car. This solution should be made on a big scale(work with a team), have place to use as a screen projector. I not recommend to make it alone

2nd solution: Digital Concert/Performance <br>
In case of a person who  is a musician or actor or works with performance. I saw a solution that someone uses an online platform to show their talent such as IG fB ZOOM . They can show individually or join with another to be a team band. So I think in the future  someone who impassion can donate by transferring money in online banking.

3rd solution: Balcony Concert/Performance <br>
In the case of a person who  is a musician or actor or works with performance. I saw another way to show their talent by using the balcony in the room to show. So I think in the future  someone who impassion can donate too by transferring money in online banking.

---

### News caster jobs
![pic](_posts/_wongsathon/assets/A3.jpg)
This job is still important in this situation. But it still has a problem too. Because they have to stay at home and protect themselves from viruses. And this time there is only this type of news, not able to do other types of news at all.

1th solution: House Production <br>
I saw some case study. They broadcast live at home by using small production (camera ,lighting , background)to make fresh news everyday.

2nd solution: Simulation Sport Caster <br>
In this situation, they are not able to do other types of news at all. Other types of news such as sport news, because every sports competition was canceled. I saw some case study. They reported the news from a football playing simulation. I think it is a good Idea and fun. Maybe they can report E-sport in the future too.

---

### Another solution to get money and safe from virus
![pic](_posts/_wongsathon/assets/4.jpg)
-You can make masks and alcohol gel for sale. Everyone  can make in their house and send by delivering parcels

-You can become to be contracted delivery because this situation everyone don’t want to go outside

-In this situation. A lot of people stay at home for a long time. They are depressed and stressed. So You can be a job who listens and gives advice for depressed people and you can do it at home. But you should have empathy skill to do this jobs

-In this situation.  A lot of people who stay at home, someone wanna go to have a haircut but they can’t. This is another opportunity  for person who have this skill to be the freelance hairdresser but you have to protect yourself when you go to customer house

---

### Analysis & Conclusion

![pic](_posts/_wongsathon/assets/5.jpg)


Sometimes it didn't have this solution. It will not make a lot of money like in the part but It can support us to have money in this time a lot of case study that we saw make us know that adapting with creativity is a good way to survive in unpredictable.We don’t have to stick with one lover but we can do multiple jobs and have plan B at the same time.


#### Questions & Feedback

[nhu.tram.veronica.tran@iaac.net](nhu.tram.veronica.tran@iaac.net)

[julia.danae.bertolaso@iaac.net](julia.danae.bertolaso@iaac.net)
