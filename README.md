![cover-1](_assets/_1-book-banner.gif) 

# Hyper Domestic X Hyper Global 

The Master in Design for Emergent Futures is proposing a revision of the publication made by the Cuban government during the 1990's, when the island was going through the special period after the fall of the Iron Curtain. 

This repository and publication has been developed by MDEF's tutors and students, understanding that the world is now in a new "special period", after the release of the COVID-19.

Knowing the limitations of mobility that the world is experiencing, there is a dramatic change in logistics and supply chains, as well as new ways to access knowledge. Right now, we are more connected than ever and we are more isolated than ever at the same time. 

The current global pandemic is opening up opportunities for hyperlocal interventions that can help people to live confinement in much better conditions while increasing long term resilience.

The exercise will consists of mapping and documenting existing practices that are happening now at the domestic scale in the world, and which people are using to solve local needs, by following open source projects or existing documentation. The work will be organized following the categories of the book "Con Nuestros Propios Esfuerzos". A second part of the exercise consists of imagining near-future scenarios, starting from January 2021, and proposing the evolution of these categories and projects in the context of the Post-Corona society. 

![cover-2](_assets/_2-book-banner.gif) 

---

## Food 

- The Rise of Sourdough by Cesar Rodriguez 
- Scarcity of Abundance by Alessio Boggero 

## Public Health

- Ethical dilemmas by Mitalee Parikh 
- Simulating of exterior realities by Pablo Zuloaga
- Protecting Masks made by #voluntaris3Dgarrotxa by Laura Freixas Conde
- Impact of Covid-19 on the health of medical personel Ergina Restou
- The Kids are not Alright by Magdalena Mojsiejuk
- Consciousness in a World at stop by Juanita Pardo
- Feels like "home": Coping with a global emergency in a foreign country by Elsa Maria Garduño Leyva
- Living in an infected world by Isaúl García

## Energy 
- Networks & Interconnectivity by Zoe
- Is there a lmit to the internet? by Caroline Rudd 

## Future of Work 

- The New Office by Natalia Barankova 
- Co-Workers at Home by Tommaso Salini
- Work Adaptive / Transformation by Wongsathon Choonhavan
- Artscapes & Expanding homes through creativity by Daphne Gerodimou
- Apps and Delivery Services by Hala Amer Adeeb Alzawaydeh
- New Services Market by Anisa Isaeva

## Education 

- Please, no more charts by Andrea Bertran López
- Directing a Home School from the Kitchen by Adel Sarvary
- Virus Prevention for all by Ching-Chia Renn

#### Credits

A project by the students of the [Masters in Design for Emergent Futures](https://mdef.gitlab.io/mdef-2019), coordinated by [@linkjournal](https://www.instagram.com/linkjournal/), in collaboration with [@iaacbcn](https://iaac.net/) [@elisavabcn](https://www.elisava.net/)[@orozaernesto](https://www.ernestooroza.com/) @fablabbcn and Fab City Global Initiative

